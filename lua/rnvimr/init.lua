-- vim.cmd('let g:rnvimr_enable_ex = 1')
vim.api.nvim_set_keymap('n', '<Space>x', ':RnvimrToggle<cr>', {noremap = true, silent = true})
