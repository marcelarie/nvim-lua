# nvim config

# TODO
1. Why this message? 
```bash
LSP[sumneko_lua][Info] Too large file: language-servers/.... The currently set size limit is: 100 KB, and the file size is: 222.1
```
2. Learn about -> airblade/vim-rooter
3. Learn about -> glepnir/dashboard-nvim
4. Learn about -> https://github.com/phaazon/hop.nvim
5. Learn about -> unblevable/quick-scope
6. Install -> https://github.com/dsznajder/vscode-es7-javascript-react-snippets
7. Learn about -> https://github.com/mattn/vim-gist
8. See if I need this -> https://github.com/xabikos/vscode-javascript
9. Search about -> https://github.com/hrsh7th/nvim-compe
10. Better auto-import for ts-server
11. Read this aboyt prettier -> https://jose-elias-alvarez.medium.com/configuring-neovims-lsp-client-for-typescript-development-5789d58ea9c
